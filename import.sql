SET GLOBAL local_infile = 1;
#SHOW GLOBAL VARIABLES LIKE 'local_infile';
SET FOREIGN_KEY_CHECKS=0;

TRUNCATE mydb.colors;
LOAD DATA LOCAL INFILE "/Users/halil/lessons/dbproject/database_tables/colors.csv"
INTO TABLE mydb.colors
COLUMNS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
ESCAPED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

TRUNCATE mydb.conditions;
LOAD DATA LOCAL INFILE "/Users/halil/lessons/dbproject/database_tables/conditions.csv"
INTO TABLE mydb.conditions
COLUMNS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
ESCAPED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

TRUNCATE mydb.genders;
LOAD DATA LOCAL INFILE "/Users/halil/lessons/dbproject/database_tables/genders.csv"
INTO TABLE mydb.genders
COLUMNS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
ESCAPED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

TRUNCATE mydb.situations;
LOAD DATA LOCAL INFILE "/Users/halil/lessons/dbproject/database_tables/situtations.csv"
INTO TABLE mydb.situations
COLUMNS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
ESCAPED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

TRUNCATE mydb.species;
LOAD DATA LOCAL INFILE "/Users/halil/lessons/dbproject/database_tables/species.csv"
INTO TABLE mydb.species
COLUMNS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
ESCAPED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

TRUNCATE mydb.breeds;
LOAD DATA LOCAL INFILE "/Users/halil/lessons/dbproject/database_tables/breeds.csv"
INTO TABLE mydb.breeds
COLUMNS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
ESCAPED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

TRUNCATE mydb.animals;
LOAD DATA LOCAL INFILE "/Users/halil/lessons/dbproject/database_tables/animals.csv"
INTO TABLE mydb.animals
COLUMNS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
ESCAPED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

TRUNCATE mydb.animals;
LOAD DATA LOCAL INFILE "/Users/halil/lessons/dbproject/database_tables/animals.csv"
INTO TABLE mydb.animals
COLUMNS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
ESCAPED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

TRUNCATE mydb.shelter_logs;
LOAD DATA LOCAL INFILE "/Users/halil/lessons/dbproject/database_tables/shelter_logs.csv"
INTO TABLE mydb.shelter_logs
COLUMNS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
ESCAPED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;
SET FOREIGN_KEY_CHECKS=1;
show warnings
