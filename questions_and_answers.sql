#1-How many domestic shorthair mix cats were taken to the shelter after 2014?
SELECT COUNT(*) as 'cats_count' from shelter_logs sl JOIN animals a ON sl.animal_keycard_id = a.keycard_id JOIN breeds b ON a.breed_id = b.id JOIN species s ON s.id = b.species_id WHERE YEAR(sl.intake_at) > 2010 AND b.name = "Domestic Shorthair Mix" AND s.name = "Cat";

#2-How many dogs maximum 1 year old have been taken into the shelter ?
SELECT COUNT(*) as 'dog_count' from shelter_logs_extra_info sl JOIN animals a ON sl.animal_keycard_id = a.keycard_id JOIN breeds b ON a.breed_id = b.id JOIN species s ON s.id = b.species_id WHERE sl.age_upon_intake_year <= 1 AND s.name = "Dog";

#3-How many stray an intact cats came to the shelter ?
SELECT COUNT(*) as 'stray_cat_count' from shelter_logs sl JOIN animals a ON sl.animal_keycard_id = a.keycard_id JOIN breeds b ON a.breed_id = b.id JOIN species s ON s.id = b.species_id JOIN situations si ON si.id = sl.intake_situation_id JOIN genders ig ON sl.intake_gender_id = ig.id WHERE ig.name like "Intact%" AND si.name = "Stray";

#4-How many animals were adopted(left) in total all saturdays ?
SELECT Count(*) as "outcome_animals_saturday_count" from animals a JOIN shelter_logs_extra_info sl ON sl.animal_keycard_id = a.keycard_id WHERE sl.outcome_weekday = "Saturday";

#5-how many injured animals were taken in the shelter GROUP BY species type ?
SELECT COUNT(*) as 'count', s.name as 'species_type' from shelter_logs sl JOIN animals a ON sl.animal_keycard_id = a.keycard_id JOIN conditions c ON sl.intake_condition_id = c.id JOIN breeds b ON a.breed_id = b.id JOIN species s ON s.id = b.species_id WHERE c.name = "Injured" GROUP BY s.id ,s.name;

#6-List the species and breeds according to the number of abandonments
# tüm türler terkedilme srasna göre listeleycem tür + cins
SELECT COUNT(*) as 'count', s.name as 'specy_type', b.name as 'breed_name' from shelter_logs sl JOIN animals a ON sl.animal_keycard_id = a.keycard_id JOIN conditions c ON sl.intake_condition_id = c.id JOIN breeds b ON a.breed_id = b.id JOIN species s ON s.id = b.species_id JOIN situations si ON si.id = sl.intake_situation_id WHERE si.name = "Owner Surrender" GROUP BY s.id , s.name, b.id, b.name ORDER BY count DESC;

#7-How many animals between the ages of 5 and 10 were left in 2017 ?
SELECT COUNT(*) as 'count' from shelter_logs_extra_info sl where sl.age_upon_outcome_year BETWEEN 5 and 10 AND outcome_year = 2017;

#8-What is the average age of cats left (abandoned)by their owners ?
SELECT FORMAT(AVG(sl.age_upon_intake_year),1) as 'avg_age' from shelter_logs_extra_info sl JOIN animals a ON sl.animal_keycard_id = a.keycard_id JOIN breeds b ON a.breed_id = b.id JOIN species s ON s.id = b.species_id JOIN situations si ON si.id = sl.intake_situation_id WHERE si.name = "Owner Surrender" AND s.name = "Cat";

#9-How many dogs that are minimum 1 year old have left the shelter ?
SELECT COUNT(*) as 'dog_count' from shelter_logs_extra_info sl JOIN animals a ON sl.animal_keycard_id = a.keycard_id JOIN breeds b ON a.breed_id = b.id JOIN species s ON s.id = b.species_id WHERE sl.age_upon_intake_year > 1 AND s.name = "Dog";

#10-•Max days did the male animals stay in the shelter between 2017 and 2018.
SELECT MAX(sl.time_in_shelter_days) as 'max_stay_days' from shelter_logs_extra_info sl JOIN animals a ON sl.animal_keycard_id = a.keycard_id JOIN breeds b ON a.breed_id = b.id JOIN species s ON s.id = b.species_id JOIN genders ig ON sl.intake_gender_id = ig.id WHERE s.name = "Dog" AND ig.name like "%Male" AND sl.intake_year BETWEEN 2017 and 2020 ;
