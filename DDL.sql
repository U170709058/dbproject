-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 COLLATE utf8_turkish_ci ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`colors`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`colors` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`species`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`species` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`breeds`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`breeds` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `species_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_breeds_animal_species1_idx` (`species_id` ASC) VISIBLE,
  CONSTRAINT `fk_breeds_animal_species1`
    FOREIGN KEY (`species_id`)
    REFERENCES `mydb`.`species` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`animals`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`animals` (
  `keycard_id` VARCHAR(10) NOT NULL,
  `color_id` INT NOT NULL,
  `breed_id` INT NOT NULL,
  `birth_at` DATETIME NOT NULL,
  PRIMARY KEY (`keycard_id`),
  INDEX `fk_animals_colors1_idx` (`color_id` ASC) VISIBLE,
  INDEX `fk_animals_breeds1_idx` (`breed_id` ASC) VISIBLE,
  CONSTRAINT `fk_animals_colors1`
    FOREIGN KEY (`color_id`)
    REFERENCES `mydb`.`colors` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_animals_breeds1`
    FOREIGN KEY (`breed_id`)
    REFERENCES `mydb`.`breeds` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
ROW_FORMAT = Default;


-- -----------------------------------------------------
-- Table `mydb`.`genders`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`genders` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`conditions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`conditions` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`situations`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`situations` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`shelter_logs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`shelter_logs` (
  `id` INT NOT NULL,
  `animal_keycard_id` VARCHAR(10) NOT NULL,
  `intake_gender_id` INT NOT NULL,
  `outcome_gender_id` INT NOT NULL,
  `intake_condition_id` INT NOT NULL,
  `intake_situation_id` INT NOT NULL,
  `age_upon_intake_day` INT NOT NULL,
  `age_upon_outcome_day` INT NOT NULL,
  `intake_at` DATETIME NOT NULL,
  `outcome_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_animals_shelter_log_animals1_idx` (`animal_keycard_id` ASC) VISIBLE,
  INDEX `fk_animals_shelter_log_genders1_idx` (`intake_gender_id` ASC) VISIBLE,
  INDEX `fk_animals_shelter_log_genders2_idx` (`outcome_gender_id` ASC) VISIBLE,
  INDEX `fk_animals_shelter_log_conditions1_idx` (`intake_condition_id` ASC) VISIBLE,
  INDEX `fk_animals_shelter_log_situations1_idx` (`intake_situation_id` ASC) VISIBLE,
  CONSTRAINT `fk_animals_shelter_log_animals1`
    FOREIGN KEY (`animal_keycard_id`)
    REFERENCES `mydb`.`animals` (`keycard_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_animals_shelter_log_genders1`
    FOREIGN KEY (`intake_gender_id`)
    REFERENCES `mydb`.`genders` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_animals_shelter_log_genders2`
    FOREIGN KEY (`outcome_gender_id`)
    REFERENCES `mydb`.`genders` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_animals_shelter_log_conditions1`
    FOREIGN KEY (`intake_condition_id`)
    REFERENCES `mydb`.`conditions` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_animals_shelter_log_situations1`
    FOREIGN KEY (`intake_situation_id`)
    REFERENCES `mydb`.`situations` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `mydb` ;

-- -----------------------------------------------------
-- Placeholder table for view `mydb`.`shelter_logs_extra_info`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`shelter_logs_extra_info` (`id` INT, `animal_keycard_id` INT, `intake_gender_id` INT, `outcome_gender_id` INT, `intake_condition_id` INT, `intake_situation_id` INT, `age_upon_intake_day` INT, `age_upon_outcome_day` INT, `intake_at` INT, `outcome_at` INT, `intake_year` INT, `intake_month` INT, `intake_day` INT, `outcome_year` INT, `outcome_month` INT, `outcome_day` INT, `age_upon_intake_year` INT, `age_upon_intake_month` INT, `time_in_shelter` INT, `time_in_shelter_days` INT, `intake_weekday` INT, `intake_hour` INT, `outcome_weekday` INT, `outcome_hour` INT, `age_upon_outcome_year` INT, `age_upon_outcome_month` INT);

-- -----------------------------------------------------
-- Placeholder table for view `mydb`.`animals_extra_info`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`animals_extra_info` (`keycard_id` INT, `color_id` INT, `breed_id` INT, `birth_at` INT, `birth_year` INT, `birth_month` INT, `birth_day` INT);

-- -----------------------------------------------------
-- View `mydb`.`shelter_logs_extra_info`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`shelter_logs_extra_info`;
USE `mydb`;
CREATE  OR REPLACE VIEW `shelter_logs_extra_info` AS
SELECT *,
(YEAR(sl.intake_at)) as `intake_year`,
(MONTH(sl.intake_at)) as `intake_month`,
(DAY(sl.intake_at)) as `intake_day`,
(YEAR(sl.outcome_at)) as `outcome_year`,
(MONTH(sl.outcome_at)) as `outcome_month`,
(DAY(sl.outcome_at)) as `outcome_day`,
(sl.age_upon_intake_day / 365) as `age_upon_intake_year`,
(sl.age_upon_intake_day / 30) as `age_upon_intake_month`,
(TIMEDIFF(sl.outcome_at, sl.intake_at)) as `time_in_shelter`,
(DATEDIFF(sl.outcome_at, sl.intake_at)) as `time_in_shelter_days`,
(DAYNAME(sl.intake_at)) as `intake_weekday`,
(HOUR(sl.intake_at)) as `intake_hour`,
(DAYNAME(sl.outcome_at)) as `outcome_weekday`,
(HOUR(sl.outcome_at)) as `outcome_hour`,
(sl.age_upon_outcome_day / 365) as `age_upon_outcome_year`,
(sl.age_upon_outcome_day / 30) as `age_upon_outcome_month`
from shelter_logs sl;

-- -----------------------------------------------------
-- View `mydb`.`animals_extra_info`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`animals_extra_info`;
USE `mydb`;
CREATE  OR REPLACE VIEW `animals_extra_info` AS
SELECT *,
(YEAR(a.birth_at)) as `birth_year`,
(MONTH(a.birth_at)) as `birth_month`,
(DAY(a.birth_at)) as `birth_day`
from animals a;


-- -----------------------------------------------------
-- Stored Procedure `mydb`.`animalCountByAnimalYearAndAnimalCount`
-- -----------------------------------------------------
USE `mydb`;
CREATE PROCEDURE `mydb`.`animalCountByAnimalYearAndAnimalCount`(INOUT in_year_out_count INT, IN animal_type INT))
BEGIN
    SELECT COUNT(*) into in_year_out_count
    from shelter_logs_extra_info sl
    JOIN animals a ON sl.animal_keycard_id = a.keycard_id
    JOIN breeds b ON a.breed_id = b.id JOIN species s ON s.id = b.species_id
    WHERE sl.age_upon_intake_year <= in_year_out_count AND s.id = animal_type;
END


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
